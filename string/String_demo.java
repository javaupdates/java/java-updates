package string;

public class String_demo {

	public static void main(String[] args) {
		String_demo sd = new String_demo();
		sd.string1();
	}

	private void string1() {
			String name = "  Ram Ram  ";
			System.out.println(name.replace('r','s'));
			System.out.println(name);
			System.out.println(name.replaceFirst("ram","sam"));
			System.out.println(name.replaceAll("ram", "sam"));  
			System.out.println(name.strip());
			System.out.println(name.stripLeading()+"----");
			System.out.println(name.stripTrailing()+"----");
			System.out.println(name.trim());
			System.out.println(name.toLowerCase());
			System.out.println(name.toUpperCase());
			System.out.println(('\u0021'+"dharshini").trim());
			System.out.println(('\u0021'+"dharshini").strip());
	}

}
