package string;

public class Stringbuffer {

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("");
		System.out.println(sb.hashCode());
		for(char i='a';i<='z';i++) {
			sb.append(i);//append means adding at the end
		}
		System.out.println(sb);
		System.out.println(sb.hashCode());
		
		StringBuffer sb1 = new StringBuffer("abcd");
		sb1.append("efg");
		sb1.append(100);
		sb1.insert(4, 123);
		sb1.reverse();
		System.out.println(sb1);
		System.out.println('\"'+"abcd"+'\"'); //print "" 
		
	}

}
