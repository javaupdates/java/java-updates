package casting;

public class Emp extends Std {

	public static void main(String[] args) {
		Emp em = new Emp();
		Std s = em;//up casting
		s.study();
		
		Std ss = new Emp();
		Emp e = (Emp) ss;//downcasting
		e.study();
		e.doProject();
	}
	public void doProject() {
		System.out.println("final year project");
	}

}
