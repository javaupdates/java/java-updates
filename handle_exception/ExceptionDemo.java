package handle_exception;
import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter no1: ");
		int no1 = sc.nextInt();
		System.out.println("Enter no2: ");
		int no2 = sc.nextInt();
		//ExceptionDemo ed = new ExceptionDemo();
		add(no1,no2);
		div(no1,no2);
	}

	private static void add(int no1,int no2) {
				System.out.println(no1+no2);
	}

	private static void div(int no1, int no2) {
		try {
			System.out.println(no1/no2);
			int[] ar = new int[no1];
			for(int i=0;i<10;i++) {
				System.out.println(ar[i]);
			}
		}
		catch(ArithmeticException ae){
			ae.printStackTrace();
			//System.out.println("Error occured");
		}
		catch(NegativeArraySizeException nas){
			System.out.println("check array length");
		}
		catch(ArrayIndexOutOfBoundsException bound) {
			System.out.println("out of bound");
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("finally block");
		}
				
	}

}
