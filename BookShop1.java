public class BookShop1
{
	//no args constructor
public BookShop1()
{
	System.out.println("Hi I am no-args constructor");
}
	public BookShop1(int no)
{
	System.out.println("Hi I am one-args constructor");
}
	public BookShop1(String str)
{
	System.out.println("Hi I am one-args constructor");
}
public static void main(String args[])
{
	BookShop1 book1 = new BookShop1();
	BookShop1 book2 = new BookShop1(10);
	BookShop1 book3 = new BookShop1("abc");
	
}
}