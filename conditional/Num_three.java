package conditional;

public class Num_three {

	public static void main(String[] args) {
		int a = 10;
		int b=79;
		int c=100;
		String str = (a>b)?(a>c)?"a is greater":"b is greater":"c is greater";
		System.out.println(str);
		
		int mark=79;
		int internal = 20;
		int attendance=75;
		
		if(mark>=80) {
			if(internal>=20) {
				System.out.println("pass");
			}else {
				System.out.println("fail");
			}
		}else {
			System.out.println("fail");
		}
		
		String result=((mark>=80)?((internal>=20)?((attendance!=75)?"pass":"fail"):"fail"):"fail");
		System.out.println(result);
		Num_three.learn_switch();
}
	public static void learn_switch() {
		int day = 2;
		switch (day) {
		  case 1://day==1
		    System.out.println("Monday");
		    break;
		  case 2:
		    System.out.println("Tuesday");
		    break;
		  case 3:
		    System.out.println("Wednesday");
		    break;
		  case 4:
		    System.out.println("Thursday");
		    break;
		  case 5:
		    System.out.println("Friday");
		    break;
		  case 6:
		    System.out.println("Saturday");
		    break;
		  case 7:
		    System.out.println("Sunday");
		    break;
		}
		// Outputs "Thursday" (day 4)


	}

}
